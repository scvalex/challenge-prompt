challenge-prompt
================

> `challenge-prompt` makes the user pause before doing something dangerous.

[![Crates.io](https://img.shields.io/crates/v/challenge-prompt.svg)](https://crates.io/crates/challenge-prompt)

## Command line tool

Install:

```
$ cargo install challenge-prompt  # with cargo
$ nix profile install 'git+https://gitlab.com/abstract-binary/challenge-prompt'  # with nix
```
    
Run:

```
$ challenge-prompt
Solve: (5 + 15) mod 5 = ?
```
    
### Usage

```
Usage: challenge-prompt [-a] [-p] [--phrase <phrase>] [-y] [-v] [--retries <retries>]

Make the user pause before doing something dangerous.

Options:
  -a, --arithmetic  prompt the user to solve a simple arithmetic problem
                    (default challenge)
  -p, --mistake     prompt the user to type in "I am probably making a mistake."
  --phrase          prompt the user to type in exactly the given phrase
  -y, --yes         prompt the user to type in 'y' or 'yes'
  -v, --version     print version information
  --retries         how many retries to allow before failing (default: 1)
  --help            display usage information

Notes:
  Set environment variable CHALLENGE_PROMPT_SEED to an u32 to fix the RNG seed.

Error codes:
  0 User passed the challenge.
  1 User failed the challenge or an error was encountered.
```

## Library example

Add this to your `Cargo.toml`:

    [dependencies]
    challenge-prompt = "0.4"

and this to your crate root:

```rust
extern crate challenge_prompt;
```
    
Basic example:

```rust
extern crate challenge_prompt;
    
let mut rng = challenge_prompt::Rng::new_from_time().unwrap();
if !challenge_prompt::Challenge::Arithmetic.prompt(&mut rng) {
   panic!("user failed the challenge")
}
```

See also the full [documentation](https://docs.rs/challenge-prompt/).
