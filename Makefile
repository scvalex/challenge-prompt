.PHONY: build release watch test clean clippy

build:
	cargo build

release:
	cargo build --release
	strip target/release/challenge-prompt

watch:
	cargo watch -x check

test:
	cargo test

clean:
	git clean -dxf

clippy:
	cargo clippy
